﻿using System;

namespace Abiture.CICDTest
{
    public class Program
    {
        /// <summary>
        /// Main
        /// </summary>
        /// <param name="args"></param>
        /// <exception cref="System.IO.IOException"></exception>
        static void Main(string[] args)
        {
            Console.WriteLine(float.MaxValue);
        }

        public static int Sum(int a,int b)
        {
            return a + b;
        }
    }
}
